const fs=require('fs')

function operations(){
    try{
        fs.readFile("../data.json",'utf-8',(err,data)=>{
            if(err){
                console.error(err)
            }
            else{
                data =  JSON.parse(data)

                const ids= [2, 13, 23]

                console.log("data read successfully")

                const ids_information=data.employees.reduce((arr,curr)=>{
                    if(ids.includes(curr.id)){
                        arr.push(curr)
                    }
                    return arr
                },[])
                fs.writeFile("../output/output1.json",JSON.stringify(ids_information),(err)=>{
                    if(err){
                        console.error(err)
                    }
                    else{
                        console.log("retrived data written successfully")
                        
                        const group_employee=data.employees.reduce((obj,each)=>{
                            if(obj[each.company]){
                                obj[each.company].push(each)
                            }
                            else{
                                obj[each.company]=[each]
                            }
                            return obj
                        },{})
                        
                        fs.writeFile("../output/output2.json",JSON.stringify(group_employee),(err)=>{
                            if(err){
                                console.error(err)
                            }
                            else{
                                console.log("grouped employees written successfully")
                                
                                const Powerpuff=group_employee["Powerpuff Brigade"]

                                fs.writeFile("../output/output3.json",JSON.stringify(Powerpuff),(err)=>{
                                    if(err){
                                        console.error(err)
                                    }
                                    else{
                                        console.log("company Powerpuff Brigade data extracted successfully")
                                        let data1=JSON.parse(JSON.stringify(data))
                                        
                                        let deleted=todelete(Object.entries(data1)[0][1],2)

                                        function todelete(arr,id){
                                            let index=arr.findIndex((obj) => obj.id === id);
                                            arr.splice(index,1)
                                            return arr
 
                                        }
                                        fs.writeFile("../output/output4.json",JSON.stringify(deleted),(err)=>{
                                            if(err){
                                                console.error(err)
                                            }
                                            else{
                                                console.log("id 2 data deleted")

                                                const company_sorted=Object.entries(group_employee).sort()

                                                const sorted_data=company_sorted.reduce((obj,each)=>{
                                                    const sort_ids=each[1].sort(function(a,b){
                                                        return a.id - b.id
                                                    })
                                                    obj[each[0]]=sort_ids
                                                    return obj
                                                },{})
                                                fs.writeFile("../output/output5.json",JSON.stringify(sorted_data),(err)=>{
                                                    if(err){
                                                        console.error(err)
                                                    }
                                                    else{
                                                        console.log("data sorted")
                                                        let index_93=data.employees.findIndex((each)=>each.id===93)

                                                        let index_92=data.employees.findIndex((each)=>each.id==92)

                                                        let temp=data.employees[index_92].company

                                                        data.employees[index_92].company=data.employees[index_93].company

                                                        data.employees[index_93].company=temp
                                                        fs.writeFile("../output/output6.json",JSON.stringify(data),(err)=>{
                                                            if(err){
                                                                console.error(err)
                                                            }
                                                            else{
                                                                console.log("data swaped")

                                                                const date= new Date().toLocaleDateString()

                                                                data.employees.forEach((each)=>{
                                                                    if(each.id%2==0){
                                                                        each['Birthday']=date
                                                                    }
                                                                })
                                                                fs.writeFile("../output/output7.json",JSON.stringify(data),(err)=>{
                                                                    if(err){
                                                                        console.error(err)
                                                                    }
                                                                    else{
                                                                        console.log("added birthday property")
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                        
                    }
                })
            }
        })
    }
    catch(err){
        console.error(err)
    }
}
module.exports=operations